FROM openjdk:8-jdk-alpine
RUN addgroup -S mamta.siyak && adduser -S mamta.siyak -G mamta.siyak
USER mamta.siyak:mamta.siyak
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]